#pragma once

#include <iostream>
#include <cmath>
#include <glm/glm.hpp>

#include "Infinity.hpp"

using namespace inf;

class Game : public IGame{
private:
	IShaderProgram *shader;
	IShaderProgram *second_shader;
	GLuint buffer;
	GLfloat *quad_data;
	GLuint position_buffer;
	GLuint normal_buffer;
	GLuint color_buffer;

	int mouse_pos_x;
	int mouse_pos_y;

	int maxdepth;
	int samples;

	GLuint fbo;
	GLuint *texbuffers;

	/*
		Adaptive manifolds filter parameters
	*/

	GLfloat *color_data;
	GLfloat *normal_data;
	GLfloat *position_data;

	bool use_filter;

	void GenFramebuffers(void);
	void RM_Filter(float *Dh, float *Dv, float sigma);
	int GetIndex(int x, int y, int width);
	int GetImIndex(int x, int y, int width);
	float dIdx(int i, int size);
	float dIdy(int i, int w, int size);

public:
	Game(void);

	void Load(void) override;
	void HandleInput(void) override;
	void Update(float deltatime) override;
	void Render(void) override;
	void Close(void) override;

	void OnResize(int width, int height) override;
	void OnKeyDown(SDL_Keycode key) override;
};

#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "IObject.hpp"

using namespace glm;

namespace inf{
	class ICamera{
	private:
		IObject *parent;
		vec3 position;
		vec3 up;
		vec3 target;
		mat4 projection_matrix;

	public:
		// Constructors
		ICamera(void);
		ICamera(vec3 position, vec3 target, vec3 up);

		// Getters and Setters
		void SetPosition(vec3 position);
		vec3 GetPosition(void);
		void SetTarget(vec3 target);
		vec3 GetTarget(void);
		void SetDirection(vec3 direction);
		void SetUp(vec3 up);
		vec3 GetUp(void);

		// Matrices
		void SetProjectionMatrix(const mat4 &projection_matrix);
		mat4 GetProjectionMatrix(void);
		mat4 GetViewMatrix(void);
		mat4 GetCameraMatrix(void);

		// Camera coordinate system
		vec3 Direction(void);
		vec3 Left(void);
		vec3 Up(void);
		void CorrectUp(void);

		// Hierarchy
		bool IsAttached(void);
		void AppendTo(IObject *parent);
		void Emancipate(void);

		// Movement
		void Move(vec3 movement);
		void MoveForward(float distance);
		void MoveBackward(float distance);
		void MoveLeft(float distance);
		void MoveRight(float distance);
		void MoveUp(float distance);
		void MoveDown(float distance);
	};
}

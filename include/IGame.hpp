#pragma once

#include "MainWindow.hpp"

namespace inf{
	class MainWindow;

	/**
	*	This is a game interface used with Infinity
	*	Create your own game class inheriting from this class
	*		and override virtual methods
	**/
	class IGame{
	protected:
		/**
		*	Reference to the window playing the game
		*		(useful to exit the loop from within the game)
		**/
		MainWindow *window;

		/**
		*	Time elapsed since last update, in miliseconds (ms)
		**/
		float deltatime;

	public:
		IGame();
		virtual ~IGame();

		/**
		*	Attaches a inf::MainWindow to the game
		*		(useful to exit the loop from within the game)
		**/
		void AttachWindow(MainWindow *window);

		/**
		*	Called before the game loop starts, tells the game to load
		*		any resources it may need (meshes, textures, shaders...)
		**/
		virtual void Load(void) = 0;

		/**
		*	Handle any and every kind of input
		*	This is called automatically from Update()
		* Use SDL_Keycode (SDL_SCANCODE)
		* https://wiki.libsdl.org/SDL_Keycode
		**/
		virtual void HandleInput(void) = 0;

		/**
		*	Updates deltatime and calls HandleInput() automatically
		*	Override to create game update logic,
		*		but don't forget to call IGame::Update(float)
		**/
		virtual void Update(float deltatime);

		/**
		*	Renders the scene
		**/
		virtual void Render(void) = 0;

		/**
		*	Called when the loop is finished, tells the game to
		*		free any resources it may be using
		**/
		virtual void Close(void) = 0;


		/**
		*	This is called when the window is resized
		*	Override to reshape the world accordingly (projection, viewport)
		**/
		virtual void OnResize(int width, int height) = 0;

		/**
		*	Called whenever the user presses a key on the keyboard (event based)
		**/
		virtual void OnKeyDown(SDL_Keycode key) = 0;
	};
}

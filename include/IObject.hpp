#pragma once

#define GLM_FORCE_RADIANS

#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/transform.hpp>

using namespace std;
using namespace glm;

namespace inf{
	/**
	*	Infinity Object
	*		Represents objects placed in the game world
	*		Objects may be hierarchized in a tree
	**/
	class IObject{
	protected:
		IObject *parent;	// make sure parent != child to avoid infinite loops
		vector<IObject*> *children;

		/**
		*	Transformations are stored in relation
		*				to the world coordinate system
		*		Hierarchies are computed on the fly
		**/

		vec3 position;
		vec3 scale;
		quat rotation;

		void RotateHierarchily(quat rotation);

	public:

		// Constructors

		IObject(void);
		IObject(vec3 position);
		IObject(vec3 position, IObject *parent);


		// Hierarchies

		bool HasParent(void);
		bool HasChildren(void);
		IObject* GetParent(void);
		vector<IObject*>* GetChildren(void);
		void AppendChild(IObject *child);
		void operator+=(IObject *child);


		// Manipulators

		void SetPosition(vec3 position);
		void Move_World(vec3 movement);
		void Move_Local(vec3 movement);
		vec3 GetPosition(void);

		void SetScale(float scale);
		void SetScale(vec3 scale);
		void Scale(float scale);
		void Scale(vec3 scale);
		vec3 GetScale(void);

		void SetRotation(quat rotation);
		void Rotate(quat rotation);
		void RotateAroundParent(quat rotation, bool keep_facing_parent = true);
		quat GetRotation(void);
		mat4 GetRotationMatrix(void);

		mat4 GetModelMatrix(void);
	};
}

#pragma once

#define GLM_FORCE_RADIANS

#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;

namespace inf{
	class VAO{
	public:
		static const GLint Vertices = 0;
		static const GLint Colors = 1;
		static const GLint Normals = 2;
		static const GLint TextureCoordinates = 3;

		// Fragment shader outputs
		static const GLint outColors = 0;
		static const GLint outNormals = 1;
		static const GLint outPosition = 2;
	};

	enum ShaderType : GLint{
		VertexShader = GL_VERTEX_SHADER,
		GeometryShader = GL_GEOMETRY_SHADER,
		FragmentShader = GL_FRAGMENT_SHADER
	};

	class IShaderProgram{
	private:
		GLuint id;
		vector<GLuint> shaders;
		vector<tuple<string, GLuint>> uniforms;

		GLint FindUniform(string name);

	public:
		IShaderProgram(void);

		void AddShaderFromSource(ShaderType type, const char *source);
		void AddShaderFromFile(ShaderType type, string file);
		void Link(void);

		void Activate(void);
		void Deactivate(void);
		void Delete(void);

		void SetUniform(string name, GLint value);
		void SetUniform(string name, GLfloat value);
		void SetUniform(string name, GLfloat x, GLfloat y);
		void SetUniform(string name, GLfloat x, GLfloat y, GLfloat z);
		void SetUniform(string name, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
		void SetUniform(string name, vec2 value);
		void SetUniform(string name, vec3 value);
		void SetUniform(string name, const vec4 &value);
		void SetUniform(string name, mat3 value);
		void SetUniform(string name, const mat4 &value);
	};
}

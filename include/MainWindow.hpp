#pragma once

#include <iostream>
#include <SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include "IGame.hpp"

using namespace std;
using namespace glm;

namespace inf{
	class IGame;

	class MainWindow{
	private:
		const char* TITLE = "Infinity";
		const int INIT_WIDTH = 400;
		const int INIT_HEIGHT = 300;
		const int UPDATE_DELAY = 1;
		const int GL_MAJOR = 4;
		const int GL_MINOR = 0;
		const bool verbose = true;

		string title;
		vec2 size;

		IGame *game;
		float deltatime;
		bool quit;

		SDL_Window *window;
		SDL_GLContext context;

		void Initialize(void);
		void CreateWindow(void);
		void CreateContext(void);
		void SetGlAttributes(void);
		void InitGlew(void);

		void InitGame(IGame* game);
	    void HandleEvents(void);
		void Loop(void);

		void Finish(void);

	public:
		MainWindow(void);
		MainWindow(const char* title);
		~MainWindow(void);

		string GetTitle(void);
		void SetTitle(string title);
		vec2 GetSize(void);
		float GetAspectRatio(void);

		void Start(IGame *game);
		void Exit(void);

		SDL_Window* GetWindow(void);
	};
}

######################################################
#
#		Generic CPP Makefile - Khin Baptista
#
######################################################

#	Project name
EXEC = Renderer.out

#	Source files
SRC = main.cpp MainWindow.cpp Game.cpp IGame.cpp IObject.cpp\
	ICamera.cpp IShaderProgram.cpp

#	Packages used
PKGS = sdl2 glew

######################################################

#	Path to source directory
SDIR = sources

#	Path to include directory
IDIR = include

#	Directory to store object files
ODIR = objects

######################################################

#	Compiler
CC = g++

#	Compiler flags
CFLAGS = -Wall -g -std=c++11 -fopenmp

######################################################

#	Linker
LINKER = g++

#	Linker flags
LFLAGS = -fopenmp

######################################################

#	Add package flags
ifdef PKGS
	CFLAGS += `pkg-config $(PKGS) --cflags`
	LFLAGS += `pkg-config $(PKGS) --libs`
endif

######################################################

#	Object files
OBJ = $(SRC:.cpp=.o)

######################################################

#	Full path to source files
SOURCES = $(patsubst %, $(SDIR)/%, $(SRC))
OBJECTS = $(patsubst %, $(ODIR)/%, $(OBJ))

######################################################

#	Make statements

all: $(EXEC)

$(EXEC): $(OBJECTS)
	$(LINKER) -o $@ $^ $(LFLAGS)

$(ODIR)/%.o: $(SDIR)/%.cpp
	$(CC) -c -o $@ $^ -I$(IDIR) $(CFLAGS)

######################################################

.PHONY: new remove clean

new:
	mkdir -p $(SDIR) $(IDIR) $(ODIR)

remove:
	rm -rf $(SDIR) $(IDIR) $(ODIR)

clean:
	rm -f $(ODIR)/*.o $(EXEC)

run:
	./$(EXEC)

######################################################

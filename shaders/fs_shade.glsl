#version 330 core

vec3 material_Ka = vec3(0.);
vec3 material_Ks = vec3(1.);
in vec3 color;
float material_Q = 25.0f;

vec3 light_position = vec3(50., 81.6, 81.6);
vec3 light_colour = vec3(1.);
vec3 ambient_colour = vec3(0.);

uniform vec2 mouse_pos;
uniform vec2 resolution;

in vec3 vertex;
in vec3 normal;

float DotOfNormalized(vec3 a, vec3 b){
    return abs( dot( normalize(a), normalize(b) ) );
}

void main(){
	vec2 iMouse = mouse_pos;
	vec2 iResolution = resolution;

	vec3 camera_position =
		vec3((2. * (iMouse.xy==vec2(0.)?.5*iResolution.xy:iMouse.xy) / iResolution.xy - 1.) * vec2(48., 40.) + vec2(50., 40.8), 169.);

    vec3 light = normalize( light_position - vertex );
    vec3 vision = normalize( camera_position - vertex );
    vec3 reflection = reflect( light, normal );

    vec3 ambient = material_Ka * ambient_colour;
    vec3 diffuse = color * DotOfNormalized( normal, light );
    vec3 specular = material_Ks * pow( DotOfNormalized( reflection, vision ), material_Q );

    vec3 final = ambient + light_colour * ( diffuse + specular );
    //gl_FragColor = vec4( final, 1 );
	gl_FragColor = vec4(1, 1, 1, 1);
}

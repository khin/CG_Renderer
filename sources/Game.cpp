#include "Game.hpp"

#if defined(_WIN32) || defined(_WIN64)
#pragma comment(lib, "opengl32.lib")
#endif

using namespace inf;
using namespace std;
using namespace glm;

Game::Game() : IGame(){
	mouse_pos_x = 0;
	mouse_pos_y = 0;

	maxdepth = 3;
	samples = 1;

	use_filter = false;
}

void Game::Load(){
	glClearColor(0, 0, 0, 1);
	glColor3f(1, 1, 1);

	quad_data = new GLfloat[16] { -1, -1, 0, -1, 1, 0, 1, 1, 0, 1, -1, 0 };

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), quad_data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &position_buffer);
	glGenBuffers(1, &normal_buffer);
	glGenBuffers(1, &color_buffer);

	shader = new IShaderProgram();
	shader->AddShaderFromFile(ShaderType::VertexShader, "shaders/vertex01.glsl");
	shader->AddShaderFromFile(ShaderType::FragmentShader, "shaders/fs_raytrace.glsl");
	shader->Link();

	shader->SetUniform("mouse_pos", 0, 0);
	shader->SetUniform("samples", 1);
	shader->SetUniform("maxdepth", 3);
	if (window != nullptr) shader->SetUniform("resolution", window->GetSize());

	GenFramebuffers();

	second_shader = new IShaderProgram();
	second_shader->AddShaderFromFile(ShaderType::VertexShader, "shaders/vertex01.glsl");
	second_shader->AddShaderFromFile(ShaderType::FragmentShader, "shaders/fs_shade.glsl");
	second_shader->Link();

	second_shader->SetUniform("mouse_pos", 0, 0);
	if (window != nullptr)
		second_shader->SetUniform("resolution", window->GetSize());
}

void Game::GenFramebuffers(){
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	texbuffers = new GLuint[2];
	glGenTextures(3, texbuffers);

	glBindTexture(GL_TEXTURE_2D, texbuffers[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, window->GetSize().x, window->GetSize().y,
		0, GL_RGB, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(
		GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texbuffers[0], 0
	);

	glBindTexture(GL_TEXTURE_2D, texbuffers[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, window->GetSize().x, window->GetSize().y,
		0, GL_RGB, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(
		GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texbuffers[1], 0
	);

	glBindTexture(GL_TEXTURE_2D, texbuffers[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, window->GetSize().x, window->GetSize().y,
		0, GL_RGB, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(
		GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, texbuffers[2], 0
	);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
		cout << "Framebuffer complete" << endl;
}

void Game::Update(float deltatime){
	IGame::Update(deltatime);

	if (shader != nullptr)
		shader->SetUniform("elapsed_time", (GLfloat)SDL_GetTicks() / 1000.0f);
}

void Game::HandleInput(){
	//const Uint8 *keystate = SDL_GetKeyboardState(NULL);

	int new_pos_x, new_pos_y;
	const Uint32 mousestate = SDL_GetMouseState(&new_pos_x, &new_pos_y);

	if ( (mousestate & SDL_BUTTON_LEFT) &&
			(new_pos_x != mouse_pos_x || new_pos_y != mouse_pos_y) ){

		mouse_pos_x = new_pos_x;
		mouse_pos_y = new_pos_y;
		shader->SetUniform("mouse_pos", mouse_pos_x, mouse_pos_y);
	}

	//if (keystate[SDL_SCANCODE_F]) cout << "Delta time: " << deltatime << endl;
}

int inline Game::GetIndex(int x, int y, int w){
	return y * w * 3 + x * 3; // 3 dimensions
}

int inline Game::GetImIndex(int x, int y, int w){
	return y * w + x;
}

void Game::RM_Filter(float *Dh, float *Dv, float sigma){
	vec2 res = window->GetSize();
	int size = res.x * res.y;
	int i = 0, im = 0;

	float a = exp(-sqrt(2.0f) / sigma);
	float *vh = new float[size];
	float *vv = new float[size];

	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (i = 0; i < size; i++){
		vh[i] = pow(a, Dh[i]);
	}

	#ifdef USE_OMP
	#pragma omp parallel for
	#endif
	for (i = 0; i < size; i++){
		vv[i] = pow(a, Dv[i]);
	}

	i = 0;

	// Horizontal
	#ifdef USE_OMP
	#pragma omp for schedule(dynamic)
	#endif
	for (int y = 0; y < (int)res.y; y++){

		for (int x = 1; x < (int)res.x; x++){
			i = GetIndex(x, y, (int)res.x);
			im = GetImIndex(x, y, (int)res.x);

			for (int d = 0; d < 3; d++){	// dimensions
				normal_data[i + d] = normal_data[i + d] +
					vh[im] * (normal_data[i - 3 + d] - normal_data[i + d]);

				position_data[i] = position_data[i] +
					vh[im] * (position_data[i - 3 + d] - position_data[i + d]);
			}
		}

		for (int x = (int)res.x - 1; x >= 0; x--){
			i = GetIndex(x, y, (int)res.x);
			im = GetImIndex(x, y, (int)res.x);

			for (int d = 0; d < 3; d++){
				normal_data[i + d] = normal_data[i + d] +
					vh[im] * (normal_data[i + 3 + d] - normal_data[i + d]);

				position_data[i + d] = position_data[i + d] +
					vh[im] * (position_data[i + 3 + d] - position_data[i + d]);
			}
		}

	}

	// Vertical
	#ifdef USE_OMP
	#pragma omp for schedule(dynamic)
	#endif
	for (int x = 0; x < (int)res.x; x++){

		for (int y = 1; y < (int)res.y; y++){
			i = GetIndex(x, y, (int)res.x);
			im = GetImIndex(x, y, (int)res.x);

			for (int d = 0; d < 3; d++){
				normal_data[i + d] = normal_data[i + d] +
					vv[im] * (normal_data[i - (int)res.x * 3 + d] - normal_data[i + d]);

				position_data[i + d] = position_data[i + d] +
					vv[im] * (position_data[i - (int)res.x * 3 + d] - position_data[i + d]);
			}
		}

		for (int y = (int)res.y - 1; y >= 0; y--){
			i = GetIndex(x, y, (int)res.x);
			im = GetImIndex(x, y, (int)res.x);

			for (int d = 0; d < 3; d++){
				normal_data[i + d] = normal_data[i + d] +
					vv[im] * (normal_data[i + (int)res.x * 3 + d] - normal_data[i + d]);

				position_data[i + d] = position_data[i + d] +
					vv[im] * (position_data[i + (int)res.x * 3 + d] - position_data[i + d]);
			}
		}

	}
}

float Game::dIdx(int i, int size){
	if (i >= size) return 0;

	vec3 dIcdx_pos;
	dIcdx_pos.x = position_data[i + 3] - position_data[i];
	dIcdx_pos.y = position_data[i + 3 + 1] - position_data[i + 1];
	dIcdx_pos.z = position_data[i + 3 + 2] - position_data[i + 2];

	vec3 dIcdx_normal;
	dIcdx_normal.x = normal_data[i + 3] - normal_data[i];
	dIcdx_normal.y =  normal_data[i + 3 + 1] - normal_data[i + 1];
	dIcdx_normal.z =  normal_data[i + 3 + 2] - normal_data[i + 2];

	return pow(dIcdx_pos.x, 2.0f) + pow(dIcdx_pos.y, 2.0f) + pow(dIcdx_pos.z, 2.0f) +
		pow(dIcdx_normal.x, 2.0f) + pow(dIcdx_normal.y, 2.0f) + pow(dIcdx_normal.z, 2.0f);
}

float Game::dIdy(int i, int w, int size){
	if (i >= size) return 0;

	vec3 dIcdy_pos;
	dIcdy_pos.x = position_data[i + 3 * w] - position_data[i];
	dIcdy_pos.y =  position_data[i + 3 * w + 1] - position_data[i + 1];
	dIcdy_pos.z =  position_data[i + 3 * w + 2] - position_data[i + 2];

	vec3 dIcdy_normal;
	dIcdy_normal.x = normal_data[i + 3 * w] - normal_data[i];
	dIcdy_normal.y = normal_data[i + 3 * w + 1] - normal_data[i + 1];
	dIcdy_normal.z = normal_data[i + 3 * w + 2] - normal_data[i + 2];

	return pow(dIcdy_pos.x, 2.0f) + pow(dIcdy_pos.y, 2.0f) + pow(dIcdy_pos.z, 2.0f) +
		pow(dIcdy_normal.x, 2.0f) + pow(dIcdy_normal.y, 2.0f) + pow(dIcdy_normal.z, 2.0f);
}

void Game::Render(){
	shader->Activate();

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glVertexAttribPointer(VAO::Vertices, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(VAO::Vertices);

	GLuint buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, buffers);

	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
		std::cout << "error " << err << std::endl;

	glDrawArrays(GL_QUADS, 0, 16);

	int w = window->GetSize().x;
	int h = window->GetSize().y;

	int size = w * h * 3;
	color_data = new GLfloat[size];
	normal_data = new GLfloat[size];
	position_data = new GLfloat[size];

	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(0, 0, w, h, GL_RGB, GL_FLOAT, color_data);

	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glReadPixels(0, 0, w, h, GL_RGB, GL_FLOAT, normal_data);

	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glReadPixels(0, 0, w, h, GL_RGB, GL_FLOAT, position_data);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (!use_filter)
		glDrawPixels(w, h, GL_RGB, GL_FLOAT, color_data);

	glDisableVertexAttribArray(VAO::Vertices);
	shader->Deactivate();

	if (use_filter){
		float sigma_s = 0.375f;
		float sigma_r = 0.15f / sqrt(2.0f);

		int imsize = w * h;
		float *dHdx = new float[imsize];
		float *dVdy = new float[imsize];
// cout << sigma_s / sigma_r << endl; 9.42809 (s = 1)
// cout << sigma_s / sigma_r << endl; 3.53553 (s = 0.375)
		#pragma omp parallel for
		for (int i = 0; i < imsize; i++){
			dHdx[i] = 1 + pow(sigma_s / sigma_r, 2) * dIdx(i, imsize);
			dVdy[i] = 1 + pow(sigma_s / sigma_r, 2) * dIdy(i, w, imsize);
		}

		RM_Filter(dHdx, dVdy, sigma_s);

		second_shader->Activate();
		glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), position_data, GL_STATIC_DRAW);
		glVertexAttribPointer(VAO::Vertices, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), normal_data, GL_STATIC_DRAW);
		glVertexAttribPointer(VAO::Normals, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), color_data, GL_STATIC_DRAW);
		glVertexAttribPointer(VAO::Colors, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glEnableVertexAttribArray(VAO::Vertices);
		glEnableVertexAttribArray(VAO::Normals);
		glEnableVertexAttribArray(VAO::Colors);

		glDrawArrays(GL_POINTS, 0, imsize);

		glDisableVertexAttribArray(VAO::Vertices);
		glDisableVertexAttribArray(VAO::Normals);
		glDisableVertexAttribArray(VAO::Colors);

		second_shader->Deactivate();
	}

	if (color_data != nullptr) delete color_data;
	if (normal_data != nullptr) delete normal_data;
	if (position_data != nullptr) delete position_data;
}

void Game::Close(){
	glDeleteFramebuffers(1, &fbo);

	shader->Delete();
	delete shader;
	window = nullptr;
}

void Game::OnResize(int w, int h){
	glViewport(0, 0, w, h);

	if (shader != nullptr)
		shader->SetUniform("resolution", w, h);
}

void Game::OnKeyDown(SDL_Keycode key){
	// Use SDL_Key (SDLK)
	// http://sdl.beuc.net/sdl.wiki/SDLKey
	switch (key){

		case SDLK_ESCAPE:
			window->Exit(); break;

		case SDLK_q:
			shader->SetUniform("samples", ++samples); break;

		case SDLK_a:
			shader->SetUniform("samples", --samples); break;

		case SDLK_w:
			shader->SetUniform("maxdepth", ++maxdepth); break;

		case SDLK_s:
			shader->SetUniform("maxdepth", --maxdepth); break;

		case SDLK_z:
			use_filter = !use_filter;
			cout << "Filter is " << (use_filter ? "on" : "off") << endl;
			break;

		case SDLK_x:
			cout << "Delta time: " << deltatime << endl;
			cout << "Samples per pixel: " << samples << endl;
			cout << "Max depth per ray: " << maxdepth << endl; break;

		default: break;
	}
}

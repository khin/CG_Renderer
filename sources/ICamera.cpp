#include "ICamera.hpp"

using namespace glm;
using namespace inf;


/* Constructors */

ICamera::ICamera() : ICamera::ICamera(vec3(0.0f, 0.0f, 0.0f),
						vec3(0.0f, 0.0f, -1.0f), vec3(0.0f, 1.0f, 0.0f)){ }

ICamera::ICamera(vec3 position, vec3 target, vec3 up){
	this->position = position;
	this->target = target;
	this->up = up;
}


/* Setters */

void ICamera::SetPosition(vec3 position){
	if (IsAttached())
		this->position = position - parent->GetPosition();
	else
		this->position = position;
}

void ICamera::SetTarget(vec3 target){
	if (IsAttached())
		this->target = target - parent->GetPosition();
	else
		this->target = target;
}

void ICamera::SetDirection(vec3 direction){
	this->SetTarget(this->GetPosition() + direction);
}

void ICamera::SetUp(vec3 up){
	this->up = up;
}


/* Getters */

vec3 ICamera::GetPosition(){
	if (IsAttached())
		return parent->GetPosition() +
			rotateY(position, yaw(parent->GetRotation()));
	else
		return position;
}

vec3 ICamera::GetTarget(){
	if (IsAttached())
		return parent->GetPosition() +
			rotateY(target, yaw(parent->GetRotation()));
	else
		return target;
}

vec3 ICamera::GetUp(){
	return up;
}


/* Matrices */

void ICamera::SetProjectionMatrix(const mat4 &projection){
	this->projection_matrix = projection;
}

mat4 ICamera::GetProjectionMatrix(){
	return projection_matrix;
}

mat4 ICamera::GetViewMatrix(){
	return lookAt(GetPosition(), GetTarget(), up);
}

mat4 ICamera::GetCameraMatrix(){
	return GetProjectionMatrix() * GetViewMatrix();
}


/* Camera coordinate system */

vec3 ICamera::Direction(){
	return glm::normalize(GetTarget() - GetPosition());
}

vec3 ICamera::Left(){
	return glm::normalize(glm::cross(up, Direction()));
}

vec3 ICamera::Up(){
	return glm::normalize(glm::cross(Direction(), Left()));
}

void ICamera::CorrectUp(){
	this->SetUp(this->Up());
}


/* Hierarchy */

bool ICamera::IsAttached(){
	return parent != nullptr;
}

void ICamera::AppendTo(IObject *object){
	this->parent = object;

	this->SetPosition(position);
	this->SetTarget(target);
}

void ICamera::Emancipate(){
	parent = nullptr;
}


/* Movement */

void ICamera::Move(vec3 movement){
	this->position += movement;
	this->target += movement;
}

void ICamera::MoveForward(float distance){
	this->Move(distance * Direction());
}

void ICamera::MoveBackward(float distance){
	this->Move(-distance * Direction());
}

void ICamera::MoveLeft(float distance){
	this->Move(distance * Left());
}

void ICamera::MoveRight(float distance){
	this->Move(-distance * Left());
}

void ICamera::MoveUp(float distance){
	this->Move(distance * Up());
}

void ICamera::MoveDown(float distance){
	this->Move(-distance * Up());
}

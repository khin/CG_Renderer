#include "IGame.hpp"

using namespace inf;

IGame::IGame(){
	deltatime = 0;
}

IGame::~IGame(){
	window = nullptr;
}

void IGame::AttachWindow(MainWindow *window){
	this->window = window;
}

void IGame::Update(float dt){
	deltatime = dt;

	HandleInput();
}

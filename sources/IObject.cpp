#include "IObject.hpp"

using namespace std;
using namespace glm;
using namespace inf;


/* Constructors */

IObject::IObject(){
	position = vec3(0.0f, 0.0f, 0.0f);
	scale = vec3(1.0f, 1.0f, 1.0f);
	rotation = quat();
}

IObject::IObject(vec3 position) : IObject::IObject(){
	this->position = position;
}

IObject::IObject(vec3 position, IObject *parent) : IObject::IObject(){
	if (parent == nullptr){
		cerr << "Error creating IObject - parent mustn't be null! ";
		cerr << "IObject created without a parent." << endl;
		this->position = position;
	}

	this->parent = parent;
	this->position = position - parent->GetPosition();
}


/* Hierarchies */

bool IObject::HasParent(){
	return parent != nullptr;
}

bool IObject::HasChildren(){
	return children != nullptr;
}

IObject* IObject::GetParent(){
	return parent;
}

vector<IObject*>* IObject::GetChildren(){
	return children;
}

void IObject::AppendChild(IObject *child){
	if (child == nullptr){
		cerr << "Child reference mustn't be nullptr" << endl;
		return;
	}

	if (child == this || child == parent){
		cerr << "Child reference mustn't be equal to object or parent" << endl;
		return;
	}

	if (!this->HasChildren())
		children = new vector<IObject*>();

	child->parent = this;
	children->push_back(child);
}

void IObject::operator+=(IObject *child){
	this->AppendChild(child);
}


/* Manipulators */

/* Translation */

void IObject::SetPosition(vec3 position){
	if (HasParent())
		this->position = position - parent->GetPosition();
	else
		this->position = position;
}

void IObject::Move_World(vec3 movement){
	this->position += movement;
}

void IObject::Move_Local(vec3 movement){
	this->position += (GetRotation() * movement);
}

vec3 IObject::GetPosition(){
	if (HasParent())
		return parent->GetPosition() + this->position;
	else
		return this->position;
}

/* Scale */

void IObject::SetScale(float scale){
	this->scale = vec3(scale, scale, scale);
}
void IObject::SetScale(vec3 scale){
	this->scale = scale;
}
void IObject::Scale(float scale){
	this->scale *= scale;
}
void IObject::Scale(vec3 scale){
	this->scale *= scale;
}

vec3 IObject::GetScale(){
	return scale;
}

/* Rotation */

void IObject::SetRotation(quat rotation){
	this->rotation = rotation;

	if (!HasChildren()) return;

	for (IObject *child : *children)
		child->RotateHierarchily(rotation);
}

void IObject::Rotate(quat rotation){
	this->SetRotation(rotation * this->rotation);
}

void IObject::RotateAroundParent(quat rotation, bool self){
	if (!HasParent()){
		cerr << "This object has no parent to rotate around. ";
		cerr << "Rotating around self instead" << endl;

		this->Rotate(rotation);
		return;
	}

	this->position = rotation * this->position;

	if (self)
		this->Rotate(rotation);
}

void IObject::RotateHierarchily(quat rotation){
	this->position = rotation * this->position;

	if (!HasChildren()) return;

	for (IObject *child : *children)
		child->RotateHierarchily(rotation);
}

quat IObject::GetRotation(){
	if (HasParent())
		return this->rotation * parent->GetRotation();
	else
		return this->rotation;
}

mat4 IObject::GetRotationMatrix(){
	return mat4_cast(this->GetRotation());
}

mat4 IObject::GetModelMatrix(){
	return glm::translate(this->GetPosition()) *
			glm::scale(this->scale) *
			this->GetRotationMatrix();
}

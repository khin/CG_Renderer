#include "IShaderProgram.hpp"

using namespace std;
using namespace glm;
using namespace inf;

IShaderProgram::IShaderProgram(){
	id = glCreateProgram();
}

GLint IShaderProgram::FindUniform(string name){
	GLint location = -1;

	auto size = uniforms.size();
	for (unsigned i = 0; i < size && location == -1; ++i)
		if (get<0>(uniforms[i]) == name)
			location = get<1>(uniforms[i]);

	if (location != -1) return location;

	location = glGetUniformLocation(id, name.c_str());

	if (location != -1)
		uniforms.push_back(make_tuple(name, location));
	else
		cerr << "Uniform " << name.c_str() << " not found." << endl;

	return location;
}

void IShaderProgram::AddShaderFromSource(ShaderType type, const char *source){
	GLint statuscode = -1;
	GLchar info[255];

	GLuint shader_id = glCreateShader(type);
	glShaderSource(shader_id, 1, &source, NULL);	// BUG
	glCompileShader(shader_id);
	delete[] source;

	glGetShaderInfoLog(shader_id, 255, NULL, info);
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &statuscode);

	if (statuscode != 1){
		glDeleteShader(shader_id);

		cerr << "Error compiling shader\nStatus code " <<
			statuscode << ": " << info << endl;

		exit(1);
	}

	shaders.push_back(shader_id);
	cout << "Shader added succesfully!" << endl;
}

void IShaderProgram::AddShaderFromFile(ShaderType type, string filename){
	ifstream file (filename);

	if (!file){
		cerr << "Error opening shader file " << filename.c_str() << "\n" << endl;

		exit(1);
	}

	file.seekg(0, file.end);
	int size = file.tellg();
	file.seekg(0, file.beg);

	char *code = new char[size];
	file.read(code, size);
	file.close();
	cout << "Shader file read succesfully" << endl;

	this->AddShaderFromSource(type, code);
}

void IShaderProgram::Link(){
	GLint statuscode = -1;
	GLchar info[255];

	for (GLint shader_id : shaders)
		glAttachShader(id, shader_id);

	glBindAttribLocation(id, VAO::Vertices, "vertex");
	glBindAttribLocation(id, VAO::Colors, "color");
	glBindAttribLocation(id, VAO::Normals, "normal");
	glBindAttribLocation(id, VAO::TextureCoordinates, "texcoord");

	glBindFragDataLocation(id, VAO::outColors, "fragColor");
	glBindFragDataLocation(id, VAO::outNormals, "fragNormal");
	glBindFragDataLocation(id, VAO::outPosition, "fragPosition");

	glLinkProgram(id);
	glGetProgramInfoLog(id, 255, NULL, info);
	glGetProgramiv(id, GL_LINK_STATUS, &statuscode);

	if (statuscode != 1){
		glDeleteProgram(id);

		cerr << "Error linking program\nStatus code: " <<
			statuscode << ": " << info << endl;

		exit(1);
	}

	for (GLint shader : shaders){
		glDetachShader(id, shader);
		glDeleteShader(shader);
	}

	shaders.clear();
	cout << "Shader program " << id << " linked succesfully" << endl;
}

void IShaderProgram::Activate(){
	glUseProgram(id);
}

void IShaderProgram::Deactivate(){
	glUseProgram(0);
}

void IShaderProgram::Delete(){
	glDeleteProgram(id);
}


/* Set uniforms */

void IShaderProgram::SetUniform(string name, GLint value){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniform1i(location, value);

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, GLfloat value){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniform1f(location, value);

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, GLfloat x, GLfloat y){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniform2f(location, x, y);

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, GLfloat x, GLfloat y, GLfloat z){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniform3f(location, x, y, z);

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, GLfloat x, GLfloat y, GLfloat z, GLfloat w){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniform4f(location, x, y, z, w);

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, vec2 value){
	SetUniform(name, value.x, value.y);
}

void IShaderProgram::SetUniform(string name, vec3 value){
	SetUniform(name, value.x, value.y, value.z);
}

void IShaderProgram::SetUniform(string name, const vec4 &value){
	SetUniform(name, value.x, value.y, value.z, value.w);
}

void IShaderProgram::SetUniform(string name, mat3 value){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniformMatrix3fv(location, 1, GL_FALSE, value_ptr(value));

	glUseProgram(0);
}

void IShaderProgram::SetUniform(string name, const mat4 &value){
	glUseProgram(id);

	GLint location = FindUniform(name);
	if (location != -1)
		glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(value));

	glUseProgram(0);
}

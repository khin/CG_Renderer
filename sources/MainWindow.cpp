#include "MainWindow.hpp"

using namespace std;
using namespace glm;
using namespace inf;

/** Public methods **/

MainWindow::MainWindow(){
	deltatime = 0;
	quit = false;
	title = (char*)TITLE;
	size = vec2(INIT_WIDTH, INIT_HEIGHT);
}

MainWindow::MainWindow(const char* title) : MainWindow::MainWindow() {
	this->title = (char*)title;
}

MainWindow::~MainWindow(){
	quit = true;
}

string MainWindow::GetTitle(){
	return title;
}

void MainWindow::SetTitle(string title){
	this->title = title;
	SDL_SetWindowTitle(window, title.c_str());
}

vec2 MainWindow::GetSize(){
	return size;
}

float MainWindow::GetAspectRatio(){
	return size.x / size.y;
}

void MainWindow::Start(IGame *game){
	Initialize();
	CreateWindow();

	CreateContext();
	SetGlAttributes();

	InitGlew();
	InitGame(game);

	Loop();
}

void MainWindow::Exit(){
	quit = true;
}

SDL_Window* MainWindow::GetWindow(){
	return window;
}


/** Private methods **/

// Setting up

void MainWindow::Initialize(){
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        cout << "SDL_Init error: " << SDL_GetError() << endl;
        exit(1);
    }

	if (verbose) cout << "SDL initialized" << endl;
}

void MainWindow::CreateWindow(){
	window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED,
							SDL_WINDOWPOS_CENTERED, INIT_WIDTH, INIT_HEIGHT,
							SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	if (window == nullptr){
		cout << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
		SDL_Quit();
		exit(1);
	}

	if (verbose) cout << "Window created" << endl;
}

void MainWindow::CreateContext(){
	context = SDL_GL_CreateContext(window);

	if (context == nullptr){
		SDL_DestroyWindow(window);
		cout << "SDL_GL_CreateContext Error: " << SDL_GetError() << endl;
		SDL_Quit();
		exit(1);
	}

	if (verbose) cout << "Created OpenGL context" << endl;
}

void MainWindow::SetGlAttributes(){
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GL_MAJOR);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GL_MINOR);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
						SDL_GL_CONTEXT_PROFILE_CORE); //core profile

    if (verbose)
		cout << "OpenGL context set to version " <<
			GL_MAJOR << "." << GL_MINOR << endl;
}

void MainWindow::InitGlew(){
	glewExperimental = GL_TRUE;

	GLenum result = glewInit();

	if (result != GLEW_OK){
		cout << "GLEW Error: " << glewGetErrorString(result) << endl;
		SDL_Quit();
		exit(1);
	}

	if (verbose) cout << "Glew initialized" << endl;
}

void MainWindow::InitGame(IGame *g){
	this->game = g;
	game->AttachWindow(this);

	game->Load();
}


// Game Update

void MainWindow::HandleEvents(){
	SDL_Event event;

	while (SDL_PollEvent(&event)){
		switch (event.type){
			case SDL_QUIT:
				this->Exit();
			case SDL_KEYDOWN:
				game->OnKeyDown(event.key.keysym.sym);
				break;
			case SDL_WINDOWEVENT:
				switch (event.window.event){
					case SDL_WINDOWEVENT_RESIZED:
						size = vec2(event.window.data1, event.window.data2);
						game->OnResize(size.x, size.y);
						break;
					default: break;
				} break;
			case SDL_MOUSEMOTION:
				//MouseMove(event.motion.xrel, event.motion.yrel, deltaTime);
				break;
			case SDL_MOUSEBUTTONDOWN:
				//MouseClick(event.button.button == SDL_BUTTON_LEFT);
				break;
			default: break;
		}
	}
}

void MainWindow::Loop(){
	float ticks = 0.0f;
	float last_ticks = 0.0f;

	if (verbose) cout << "Starting main loop" << endl;
	while (!quit){
		// Update deltatime
		ticks = (float)SDL_GetTicks();
		deltatime = (ticks - last_ticks) / 1000.0f;
		last_ticks = ticks;

		HandleEvents();

		game->Update(deltatime);

		glClear(GL_COLOR_BUFFER_BIT);
		game->Render();
		SDL_GL_SwapWindow(window);

		SDL_Delay(UPDATE_DELAY);
	}

	if (verbose) cout << "Main loop finished" << endl;

	game->Close();
	Finish();
	SDL_Quit();	// causing "double free or corruption. Aborted (core dumped)"
}


// Finalize everything

void MainWindow::Finish(){
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);

	delete game;

	if (verbose) cout << "GL context and SDL window destroyed" << endl;
}

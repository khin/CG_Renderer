#include <iostream>
#include <SDL.h>

#if defined(_WIN32) || defined(_WIN64)
#undef main
#endif

//#define USE_OMP

#include "Infinity.hpp"
#include "Game.hpp"

using namespace std;
using namespace glm;
using namespace inf;

int main(int argc, const char** argv){
	inf::MainWindow *window;

	if (argc >= 2)
		window = new inf::MainWindow(argv[1]);
	else
		window = new inf::MainWindow();

	window->Start(new Game());

	return 0;
}
